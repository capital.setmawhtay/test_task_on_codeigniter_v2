$(document).ready(function() {
	function onAjaxSuccess(data) {
	    $("#form_responce").html(data);
	    $("#form_responce form").submit();
	}

	$(document).on("click", "button#send-support-project", function(e) {
		e.preventDefault();
		var baseurl = document.location.origin + "/";
		var form = $("#donate_forms");

		$.ajax({
			type: "POST",
			url: baseurl + "payment/donate_form",
			data: $("form#donate_forms").serialize(),
			dataType: "json",
			beforeSend: function() {
				$("button#send-query").attr("disabled", "disabled");
			},
			success: function(json) {
				console.log(json.error);
				$("button#send-query").attr("disabled", false);
				$(".error-valid").remove();
			
				if (json.error) {
					for (var key in json.error) {
						$('[name="' + key + '"]').after(json.error[key]);
						$('[name="' + key + '"]')
							.siblings("p")
							.addClass("error-valid");
					}
				} else if (json.form) {
					onAjaxSuccess(json.form);
				}
				//    //variant 1
				// if (json["error"]) {
				// 	if (json["error"]["email"]) {
				// 		var error_email = json["error"]["email"];
				// 		$('[name="donate_email"]').after(
				// 			"<span class='error-valid'>" + error_email + "</span>"
				// 		);
				// 	}
				// 	if (json["error"]["sum"]) {
				// 		var error_sum = json["error"]["sum"];
				// 		$('[name="donate_sum"]').after(
				// 			"<span class='error-valid'>" + error_sum + "</span>"
				// 		);
				//     }
				// } else if(json['form']){

				// 	onAjaxSuccess(json['form']);
				// }
			}
		});
	});
});
