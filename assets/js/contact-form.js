function recaptchaCallback() {
	$("div[name='error-captcha']").html("");
}

$(document).ready(function() {
	$(document).on("click", "button#send-query", function(e) {
		e.preventDefault();
		var baseurl = document.location.origin + "/";

		$.ajax({
			type: "POST",
			url: baseurl + "contacts/send_data",
			data: $("form#ajax-contact-frm").serialize(),
			dataType: "json",
			beforeSend: function() {
				$("button#send-query").attr("disabled", "disabled");
			},
			success: function(json) {
                $("button#send-query").attr("disabled", false);
                $("span#success-msg").html("");

				if (json["error"]) {
					console.log(json["error"]);

					if (json["error"]["name"]) {
						var error_name = json["error"]["name"];
						$("div[name='error-fname']").html(
							"<span>" + error_name + "</span>"
						);
						$("#fname").focus(function() {
							$("div[name='error-fname']").html("");
						});
					}

					if (json["error"]["email"]) {
						var error_email = json["error"]["email"];
						$("div[name='error-email']").html(
							"<span>" + error_email + "</span>"
						);
						$("#email").focus(function() {
							$("div[name='error-email']").html("");
						});
					}

					if (json["error"]["text"]) {
						var error_text = json["error"]["text"];
						$("div[name='error-text']").html("<span>" + error_text + "</span>");
						$("#text").focus(function() {
							$("div[name='error-text']").html("");
						});
					}

					if (json["error"]["captcha_error"]) {
						var error_captcha = json["error"]["captcha_error"];
						$("div[name='error-captcha']").html(
							"<span id='google-captcha'>" + error_captcha + "</span>"
						);
					}
				} else {
					$("span#success-msg").html(
						'<div class="alert alert-success" role="alert">Ваше повідомлення було успішно відправлене.</div>'
					);
				}
			}
		});
	});
});
