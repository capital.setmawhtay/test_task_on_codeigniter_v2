var SITEURL = document.location.origin+'/';

$(document).ready(function() {
    $('#submit_login').click(function(e) {
        e.preventDefault();

        $.ajax({
            url: SITEURL + 'admin/submit',
            type: 'POST',
            dataType: "json",
            data: $('#admin-login').serialize(),
            success: function(data) {
                var errors = "";
                data.forEach(function(error) {
                    for (var key in error) {
                        var error = '<div>' + error[key] + '</div>';
                        errors += error;
                    }
                });

                $('div.jsError').html('<div class="alert alert-danger">' +
                    errors + '</div>');

                if (errors == "") {
                    $('div.jsError').html('');
                    let href = window.location.href;
                    let href_arr = href.split('/');
                    window.location.href = href_arr[0] + '/admin/admin_page';
                } 
              // if(window.grecaptcha) grecaptcha.reset();
            }
        });
    });
});