<?php ?>

<div class="jsError"></div>

<center>
    <h2 class="mt-5">Login to the site</h2>
</center>
<div class="container main">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <form class="form-horizontal admin-login" id="admin-login">

                <div class="form-group">
                    <label for="password" class="control-label col-md-2">Password</label>
                    <div class="col-md-10">
                        <input type="password" id="admin_password" name="password" class="form-control" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-md-2">Email</label>
                    <div class="col-md-10">
                        <input type="text" name="email" id="admin_email" class="form-control" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="captcha" class="col-md-2 control-label">Captcha</label>
                    <div class="col-md-10">
                        <div class="g-recaptcha" data-sitekey="<?php echo $google_captcha_site_key; ?>"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <input type="submit" id="submit_login" name="submit" class="btn btn-default" value="Login" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
