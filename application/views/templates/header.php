<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"> </script>
	<script src="<?php echo base_url('assets/js/script_active_link.js'); ?>"> </script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<?php if ($this->uri->segment(1) == "contacts") { ?>
	<script src="<?php echo base_url(); ?>assets/js/contact-form.js"></script>
	<script>
		 infoWindowContent = JSON.parse(`<?php if(isset($infowindow)) echo ($infowindow); ?>`);
		 markerMap = JSON.parse(`<?php if(isset($infowindow)) echo ($marker); ?>`);
	</script><?php } ?>
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand text-uppercase" href="<?php echo base_url(); ?>">My blog</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item font-weight-bold"><a class="nav-link" href="<?php echo base_url(); ?>"><?php echo lang('home'); ?><span class="sr-only">(current)</span></a></li>
				<li class="nav-item font-weight-bold"><a class="nav-link" href="<?php echo base_url(); ?>about"><?php echo lang('about'); ?></a></li>
				<li class="nav-item font-weight-bold"><a class="nav-link" href="<?php echo base_url(); ?>contacts"><?php echo lang('contacts'); ?></a></li>
			</ul>

			<?php
			echo form_open('', array('class' => 'form-inline my-2 my-lg-0'));
			?>
			<select name="language" onchange="javascript:this.form.submit();">
				<?php
				$lang = array('english' => "English", 'ukrainian' => "Ukrainian");

				foreach ($lang as $key => $val) {
					if ($key == $language)
						echo "<option value = '" . $key . "' selected>" . $val . "</option>";
					else
						echo "<option value = '" . $key . "'>" . $val . "</option>";
				}
				?>
			</select>
			<?php
			form_close();
			?>
			<form class="form-inline my-2 my-lg-0">
				<input class="form-control mr-sm-2" type="search" id="search" name="search" placeholder="<?php echo lang('search_post'); ?>" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit"><?php echo lang('search'); ?></button>
			</form>
		</div>
	</nav>