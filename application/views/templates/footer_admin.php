
<footer id="footer">
    <div class="footer-bottom">
                <div class="text-center">Copyright © <?php echo date('Y'); ?>
                Drobotko Taras</div>
            </div>
</footer>
<?php if ($this->uri->segment(2) == "news-list") { ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
<script src="<?php echo base_url('assets/js/news_admin.js'); ?>"></script>
<?php } ?>
<?php if ($this->uri->segment(2) == "admin_login") { ?>
<script src="<?php echo base_url('assets/js/login_admin.js');?>"></script>
<?php } ?>
<script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"> </script>
<script src="<?php echo base_url('assets/js/script_active_link.js');?>"> </script>
<script src='https://www.google.com/recaptcha/api.js'></script>

</body>
</html>

