<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="title" content="">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Admin | <?php echo (isset($title)) ? $title : 'Login'; ?></title>

         <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
         <link type="text/css" href="<?php echo base_url('assets/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
         <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
         <link rel="stylesheet" href="<?php echo base_url('assets/css/croppie.css'); ?>" rel="stylesheet">
         <link type="text/css" href="<?php echo base_url('assets/css/admin_style.css');?>" rel="stylesheet">
         
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
         <script src="<?php echo base_url('assets/js/croppie.js');?>"></script>
    </head>
    <body>
    <?php  if($_SERVER['REQUEST_URI'] !== '/admin/admin_login'): ?>
    <?php if($_SERVER['REQUEST_URI'] === '/index.php/admin/admin_login')
               redirect('/admin/admin_login'); ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand text-uppercase" href="<?php echo base_url(); ?>">My blog | Admin</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
        </button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item font-weight-bold"><a class="nav-link" href="<?php echo base_url(); ?>">Blog<span class="sr-only">(current)</span></a></li>
                <li class="nav-item font-weight-bold"><a class="nav-link" href="<?php echo base_url(); ?>admin/admin_logout">Logout</a></li>
                <li class="nav-item font-weight-bold"><a class="nav-link" href="<?php echo base_url(); ?>admin/news-list">News list</a></li>
                <li class="nav-item font-weight-bold"><a class="nav-link" href="<?php echo base_url(); ?>admin/admin_page">Admins list</a></li>
			</ul>
		</div>
    </nav>
    <?php endif;?>