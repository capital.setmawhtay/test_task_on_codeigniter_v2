<section class="widget form-widget mb-3 px-3 text-center" id="donate_block">
    <h3 id="h-coffee">Допомога проекту</h3>
    <form class="form-callback" id="donate_forms" method="POST" action="<?= base_url(); ?>payment/donate_form" accept-charset="utf-8">
        <div class="form-group">
            <input class="input-field" type="text" name="donate_email" id="donate_email" placeholder="Введіть ваш email" value="<?php echo set_value('donate_email'); ?>">
            <?php echo form_error('donate_email'); ?>
        </div>
        <div class="form-group child">
            <input class="input-field" type="text" name="donate_sum" id="donate_sum" placeholder="25 грн." value="<?php echo set_value('donate_sum'); ?>">
            <?php echo form_error('donate_sum'); ?>
        </div>
        <button class="btn btn-primary" id="send-support-project">ПІДТРИМАТИ</button>
    </form>
    <span id='form_responce'></span> <!-- This is the virtual field that the form will return to. -->
</section>