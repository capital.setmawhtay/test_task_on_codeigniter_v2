<div id="main_part">
    <h2 class="h1-responsive font-weight-bolder text-center my-4"><?php echo $message; ?></h1>
        <p class="text-center">Ваше пожертвування йде на оплату хостингу і розвиток проекту, щоб ми могли і надалі
            служити вам цікавими та корисними матеріалами!</p>
        <div id="smile" class="text-center"><i class="fa fa-smile-o fa-4x"></i></div>
</div>