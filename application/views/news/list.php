<div class="container main">
   <div class="row mt40">
      <div class="col-md-5">
         <h2>List of the news</h2>
      </div>
      <div class="col-md-7">
         <a id="create-new-post" class="btn btn-default back" href="javascript:void(0)">Add New Post</a>
      </div>
      <br><br>
      <div class="col-md-12">
         <table class="table table-bordered table-striped" id="post_list">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Image</th>
                  <th>Actions</th>
               </tr>
            </thead>
            <tbody id="show_data">

            </tbody>
         </table>
      </div>
   </div>
</div>

<!-- Model for add edit posts -->
<div class="modal fade" id="ajax-post-modal" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="postCrudModal"></h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
               &times;
            </button>
         </div>
         <div class="modal-body">
            <form id="postForm" name="postForm" class="form-horizontal">
               <input type="hidden" name="post_id" id="post_id">
               <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-12">
                     <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="" required="">
                  </div>
               </div>
               <div class="form-group">
                  <label for="short_content" class="col-sm-4 control-label">Short text</label>
                  <div class="col-sm-12">
                     <textarea class="form-control" id="short_content" name="short_content" placeholder="Enter short text of the post" value="" required=""></textarea>

                  </div>
               </div>
               <div class="form-group">
                  <label for="text" class="col-sm-4 control-label">Main text</label>
                  <div class="col-sm-12">
                     <textarea class="form-control" id="text" name="text" placeholder="Enter main text of the post" value="" required=""></textarea>

                  </div>
               </div>
               <div class="form-group">
                  <label for="image" class="col-sm-5 control-label">Image (with crop)</label>
                  <div class="col-sm-12">
                     <img id="blah" src="http://www.tutsmake.com/ajax-image-upload-with-preview-in-codeigniter/" alt="your image" />
                     <div id="img_name"></div>
                     <input type="hidden" name="hid_img_name" id="hid_img_name" value="">
                     <input type="file" class="form-control" name="image_file" multiple="true" accept="image/*" id="image" onchange="readURL(this);" value="" required="">
                  </div>
               </div>
               <div class="form-group">
                  <label for="image_alt" class="col-sm-4 control-label">Image alt</label>
                  <div class="col-sm-12">
                     <input type="text" class="form-control" id="image_alt" name="image_alt" placeholder="Enter Image alt" value="" required="">
                  </div>
               </div>
               <div class="form-group">
                  <label for="slug" class="col-sm-2 control-label">Slug</label>
                  <div class="col-sm-12">
                     <input type="text" class="form-control" id="slug" name="slug" placeholder="Enter post's slug" value="" required="">
                  </div>
               </div>
               <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
                  </button>
               </div>
            </form>
         </div>
         <div class="modal-footer">
         </div>
      </div>
   </div>
</div>

<div id="uploadimageModal" class="modal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Crop Image</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-8 text-center">
                  <div id="image_demo" style="width:350px; margin-top:30px"></div>
               </div>
               <div class="col-md-4" style="padding-top:30px;">
                  <br />
                  <br />
                  <br />
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button class="btn btn-success crop_image">Crop</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>