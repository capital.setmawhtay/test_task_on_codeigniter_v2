<?php

class My_img_manipulations
{
    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI = &get_instance();
    }

    public function resize_and_compress_img($image)
    {
        $this->CI->load->config('image_lib', TRUE);
        $image_vals = $this->CI->config->item('img', 'image_lib');
        $image_vals['source_image'] = './uploads/' . $image;
        $image_vals['new_image'] = './uploads/' . $image;
        $this->CI->load->library('image_lib',  $image_vals);
        $this->CI->image_lib->resize();
    }

    /** 
     * upload cropped image (cropped with croppie)
     * $image_name use data form input with name 'image_name'
     */
    public function upload_cropped_image()
    {
        $data = $this->CI->input->post('image');
        $image_name = $this->CI->input->post('image_name');

        // break image name into array
        $image_name_arr = explode('.', $image_name);
        // get image name extension
        $ext = end($image_name_arr);

        // pop the element off the end of array
        array_pop($image_name_arr);

        // implode the rest of the array
        $image_name = implode('.', $image_name_arr);

        // get data to write in file
        $image_array_1 = explode(";", $data);
        $image_array_2 = explode(",", $image_array_1[1]);
        $data = base64_decode($image_array_2[1]);
        
        // new image name of the cropped image
        $image_name = $image_name . '_' . time() . '.' . $ext;
        
        // write data to the file
        file_put_contents('./uploads/' . $image_name, $data);
        
        // give need permissions to the file 
        chmod('./uploads/' . $image_name, 0777);

        $response = $image_name;
        echo json_encode(array('data' => $response));
    }
}
