<?php
class Search_news extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('search_news_model');
    }

    public function search()
    {
        $term = $this->input->get('term');
        $data = $this->search_news_model->search_news($term);

        echo json_encode($data);
    }
}
