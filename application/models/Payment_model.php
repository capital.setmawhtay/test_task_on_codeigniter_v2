<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model
{
    public function add_donate($data)
    {
        $query = $this->db->get_where('donate', array('order_id' => $data['order_id']));
        $count = $query->num_rows();
        if ($count === 0) {
            $this->db->insert('donate', $data);
        }
    }

    public function add_donate_status($data)
    {
        $this->db->set('status', $data['status']);
        $this->db->where('order_id', $data['order_id']);
        $this->db->update('donate');
    }
}
