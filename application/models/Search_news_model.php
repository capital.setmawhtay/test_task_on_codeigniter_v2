<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_news_model extends CI_Model
{ 
     public function search_news($term) {
 
        $this->db->like('title', $term);
 
        return $this->db->get("news")->result();
     }
}