<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 */
if(!function_exists('get_hashed_password'))
{
    function get_hashed_password($plain_password)
    {
        return password_hash($plain_password, PASSWORD_BCRYPT);
    }
}

/**
 * This function used to verify the hashed password
 * @param {string} $plainPassword : This is plain text password
 * @param {string} $hashedPassword : This is hashed password
 */
if(!function_exists('verify_hashed_password'))
{
    function verify_hashed_password($plain_password, $hashed_password)
    {
        return password_verify($plain_password, $hashed_password) ? true : false;
    }
}