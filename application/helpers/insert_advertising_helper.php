<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('insert_reclama_in_html')) {

    function insert_reclama_in_html($html)
    {
        $CI =& get_instance();
        
        $reclama = $CI->config->item('reclama');

        $strlen = mb_strlen($html);
        if ($strlen > 1000) {
            $num = round($strlen - $strlen / 3);

            //Code of the advertising
            $adsense = '<div class="row line-video" style="border-top:1px solid; position:relative; z-index:1; border-bottom: 1px; solid; padding: 10px 0; margin: 40px 0">';
            $adsense .= $reclama;
            $adsense .= "</div>";

            $html = preg_replace('@([^^]{' . $num . '}.*?)(\r?\n\r?\n|</p>)@', "\\1$adsense\\2", trim($html), 1);
        } else {
            $html .=  $reclama;
        }

        return $html;
    }
}
