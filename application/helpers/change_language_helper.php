<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('change_language')) {

    function change_language()
    {
        $CI = &get_instance();
        //Get the selected language
        if (null !==  $CI->input->post('language')) {
            $language =  $CI->input->post('language');
        } else {
            $language = 'english';
        }
        //Choose language file according to selected lanaguage
        if ($language == "english")
             $CI->lang->load('english_lang', 'english');
        else if ($language == "ukrainian")
             $CI->lang->load('ukrainian_lang', 'ukrainian');

             return $language;
    }
}
