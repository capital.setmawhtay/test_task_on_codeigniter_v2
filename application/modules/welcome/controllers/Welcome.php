<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

  public function __construct() {
        parent::__construct();
        $this->load->module('school');
    }

    public function index()
    {     
        $data['school_name'] = $this->school->get_school_name();
        $data['school_location'] = $this->school->get_school_location();
         $this->load->view('welcome_message', $data);
    }
}
