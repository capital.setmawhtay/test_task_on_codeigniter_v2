<?php

class About extends MY_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('language');
        $this->load->helper('change_language');
        //$this->load->module("template");
    }

    public function call_aboutpage() {
        $about_page = "about/about_view";
        $data['title'] = "About";

        $data['language'] = change_language();

        $this->display_front($about_page, $data);
        //$this->template->loadTemplate($about_page); 
    }
}