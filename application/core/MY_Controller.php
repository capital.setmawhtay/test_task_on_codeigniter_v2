<?php

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function display_front($page, $data)
    {
        $this->load->view('templates/header', $data);
        $this->load->view($page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function display_admin($page, $data)
    {
        $this->load->view('templates/header_admin', $data);
        $this->load->view($page, $data);
        $this->load->view('templates/footer_admin', $data);
    }
}
