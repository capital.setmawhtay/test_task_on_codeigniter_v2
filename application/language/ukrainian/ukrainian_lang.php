<?php if (!defined('BASEPATH'))
exit('No direct script access allowed');
$lang['recent_news'] = 'Останні новини';
$lang['copyright'] = 'Всі права захищені';
$lang['drobotko'] = ' Дроботько Тарас';
$lang['search'] = 'Пошук';
$lang['search_post'] = 'Пошук посту';
$lang['home'] = 'Домашня сторінка';
$lang['about'] = 'Про мене';
$lang['contacts'] = 'Контакти';